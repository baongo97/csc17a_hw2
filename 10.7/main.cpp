/**  Name: Bao Gia Ngo
 *  Student ID: 2843439
 *  Date: 9/10/2019
 *  HW: 10.7 Name Arranger
 *  Problem:Write a program that asks for the user’s first, middle, and last names. The names
 *          should be stored in three different character arrays. The program should then store,
 *          in a fourth array, the name arranged in the following manner: the last name followed
 *          by a comma and a space, followed by the first name and a space, followed by the
 *          middle name. For example, if the user entered “ Carol Lynn Smith”, it should store
 *          “ Smith, Carol Lynn” in the fourth array. Display the contents of the fourth array
 *          on the screen. 
 *  I certify this is my own work and code*/

#include <cstdlib>
#include <iostream>
#include <string>
#include <cstring>
using namespace std;

const int LENGTH = 100;

int main()
{
    char first[LENGTH], middle[LENGTH], last[LENGTH], name[LENGTH];

    // Ask for user's first, middle and last name.
    cout << "Enter your first name: ";
    cin  >> first;
    cout << "Enter your middle name: ";
    cin >> middle;
    cout << "Enter your last name: ";
    cin >> last;
        
    strcpy(name, last);
    strcat(name, ", ");
    strcat(name, first);
    strcat(name, " ");
    strcat(name, middle);

    cout << "Full Name: " << name;
    return 0;
}