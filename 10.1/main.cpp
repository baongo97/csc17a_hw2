/**  Name: Bao Gia Ngo
 *  Student ID: 2843439
 *  Date: 9/10/2019
 *  HW: 10.1 String Length
 *  Problem:Write a function that returns an integer and accepts a pointer to a C-string as 
 *          an argument. The function should count the number of characters in the string and return that
 *          number. Demonstrate the function in a simple program that asks the user to input a
 *          string, passes it to the function, and then displays the function’s return value. 
 *  I certify this is my own work and code*/

#include <cstdlib>
#include <iostream>
#include <string>
#include <cstring>
using namespace std;

int stringlen(char *);

int main()
{
	const int LENGTH = 100;
	char input[LENGTH];
	cout << "Enter a string: ";
	cin.getline(input, LENGTH);
	cout << "Length of " << input << ": " << stringlen(input) << endl;
	return 0;
}
int stringlen(char *Str)
{	
	return strlen(Str);
}


