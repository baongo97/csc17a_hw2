/**  Name: Bao Gia Ngo
 *  Student ID: 2843439
 *  Date: 9/10/2019
 *  HW: 10.8 Sum of digits in string
 *  Problem: Write a program that asks the user to enter a series of single digit numbers with nothing
 *          separating them. Read the input as a C-string or a string object. The program should
 *          display the sum of all the single-digit numbers in the string. For example, if the user
 *          enters 2514, the program should display 12, which is the sum of 2, 5, 1, and 4. The
 *          program should also display the highest and lowest digits in the string. 
 *  I certify this is my own work and code*/

#include<cstdlib>
#include<iostream>

using namespace std;

const int LENGTH = 50;
int main()
{
    char String[LENGTH];
    int number, Sum = 0, i;
    int small, large;
    cout<<"Enter a single-digit number: ";
    cin.getline(String,15,'\n');
    number = atoi(String);
    i = number % 10;
    large = i;
    small = i;
    while(number > 0)
    {  
        i = number % 10;
        if(small > i)//if getting small digit
        {
            small = i;
        }
        else if(large < i)//if getting large digit
        {   
            large=i;
        }
        Sum = Sum + i;
        number = number / 10;
        }
    cout << "Sum of digits in number is: " << Sum << endl;
    cout << "Largest number is: " << large << endl;
    cout << "Smallest number is: " << small << endl;
    return 0;
}