/**  Name: Bao Gia Ngo
 *  Student ID: 2843439
 *  Date: 9/10/2019
 *  HW: 9.10 Reverse Array
 *  Problem:Write a function that accepts an int array and the array’s size as arguments. 
 *          The function should create a copy of the array, except that the element values 
 *          should be reversed in the copy. The function should return a pointer to the new array. 
 *          Demonstrate the function in a complete program
 *  I certify this is my own work and code*/

#include <cstdlib> 
#include <iostream>
#include <iostream>

using namespace std;

int *reverse(int[], int);
void print(int[], int);

int main()
{
    int size=10; 
    int array[10] = {1,2,4,5,7,9,17,23,34,66};
    cout << "Original array: ";
    print(array,size);
    int *reversedArray=reverse(array, size);
    cout << "Reversed array: ";
    print(reversedArray, size);
    return 0;
}

int *reverse(int a[],int n)
{
    int i,j;
    if(n<=0)
    {
        return NULL;
    }
    
    int *copy = new int[n];
    for(i=0;i<n;i++)
    {
        copy[i]=a[n-i-1];
    }
    return copy;
}
void print(int a[],int n)
{
    int i;
    for(i=0;i<n;i++)
    {
        cout<<a[i] << " ";
    }
    cout << endl;
}