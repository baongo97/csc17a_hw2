/**  Name: Bao Gia Ngo
 *  Student ID: 2843439
 *  Date: 9/10/2019
 *  HW: 10.2 Backward String
 *  Problem:Write a function that accepts a pointer to a C-string as an argument and displays its
 *          contents backward. For instance, if the string argument is “ Gravity” the function
 *          should display “ ytivarG”. Demonstrate the function in a program that asks the user
 *          to input a string and then passes it to the function. 
 *  I certify this is my own work and code*/

#include <cstdlib>
#include <iostream>
#include <string>
#include <cstring>
using namespace std;

void backwardsInput(string&);

int main()
{
   string input;  
   cout << "Write a word: ";
   getline(cin, input);
   cout << "Word in reverse: ";
   backwardsInput(input);  
   return(0);
}

void backwardsInput(string& backwards)
{
for(int i = backwards.length() - 1; i >= 0; i--)
{
       cout << backwards[i];
}
}