/**  Name: Bao Gia Ngo
 *  Student ID: 2843439
 *  Date: 9/10/2019
 *  HW: 9.13 Movie Statistics
 *  Problem:Write a program that can be used to gather statistical data about the 
 *          number of movies college students see in a month. The program should perform the following steps:
 *          A) Ask the user how many students were surveyed. An array of integers with this
            many elements should then be dynamically allocated.
            B) Allow the user to enter the number of movies each student saw into the array.
            C) Calculate and display the average, median, and mode of the values entered. (Use
            the functions you wrote in Problems 8 and 9 to calculate the median and mode.)
            Input Validation: Do not accept negative numbers for input.
 *  I certify this is my own work and code*/

#include <cstdlib>
#include <iostream>
using namespace std;

// Function prototype
void getData(int *, int);
void selectionSort(int *, int);
double getAvg(int *, int);
double getMedian(int *, int);
int getMode(int *, int);

int main()
{
    int *movies, students, mode;
    double avg, med;
    
    //Ask for number of student
    cout << "How many students were surveyed? ";
    cin	 >> students;

    movies = new int[students];

    getData(movies, students);

    selectionSort(movies, students);

    avg = getAvg(movies, students);

    med = getMedian(movies, students);

    mode = getMode(movies, students);

    cout << "Statistical results for the number of movies college students see in a month." << endl;
    cout << "Average: " << avg << endl;
    cout << "Median:  " << med << endl;
    cout << "Mode:    " << mode << endl;

delete [] movies;
movies = 0;

return 0;
}

// Get number of movies watched by number of students
void getData(int *array, int size)
{
    cout << "Enter the number of movies each student has seen in that month." << endl;
    for (int i = 0; i < size; i++)
    {
        cout << "Student " << (i + 1) << ": ";
	cin  >> *(array + i);
    }
}

void selectionSort(int *array, int size)
{
    int check;
    int minIndex;
    int minValue;

    for (int check = 0; check < (size - 1); check++)
    {
    	minIndex = check;
    	minValue = *(array + check);
    	for (int i = check + 1; i < size; i++)
    	{
            if (*(array + i) < minValue)
            {
                minValue = *(array + i);
		minIndex = i;
            }
	}
	*(array + minIndex) = *(array + check);
	*(array + check) = minValue;
    }
}

double getAvg(int *array, int size)
{
    double Sum = 0;
    for (int i = 0; i < size; i++)
    {
	Sum += *(array +i);
    }
    return Sum / size;
}

double getMedian(int *array, int size)
{
    int mid = (size - 1) / 2;
    double med;
    if (size % 2 == 0)
    {
        med = (*(array + mid) + *(array + (mid + 1))) / 2;
    }
    else
        med = *(array + mid);
    return med;
}

int getMode(int *array, int size)
{
    int Mode, Most, Count;
    Count = Most = 0;
    for (int i = 0; i < size; i++)
    {
        Count++;
        if (*(array + i) < *(array + i + 1))
        {
            if (Count > Most)
            {
                Mode = *(array + i);
                Most = Count;
            }
            Count = 0;
        }
    }
    return Mode;
}